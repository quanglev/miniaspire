<?php

namespace Database\Factories;

use App\Models\Loan;
use App\Models\LoanStatus;
use App\Models\LoanTermUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Loan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => random_int(1, 2),
            'amount' => $this->faker->numberBetween(1, 100000),
            'term' => $this->faker->numberBetween(2, 20),
            'term_unit_id' => $this->faker->randomElement([LoanTermUnit::LOAN_TERM_UNIT_WEEK]),
            'status_id' => $this->faker->randomElement([
                LoanStatus::LOAN_STATUS_OPEN,
                LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED,
                LoanStatus::LOAN_STATUS_APPROVED,
                LoanStatus::LOAN_STATUS_REJECTED
            ]),
            'created_at' => $this->faker->dateTime
        ];
    }
}
