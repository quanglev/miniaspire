<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('approver_id')->nullable();
            $table->decimal('amount');
            $table->integer('term');
            $table->integer('term_unit_id')->default(\App\Models\LoanTermUnit::LOAN_TERM_UNIT_WEEK);
            $table->string('status_id')->default(\App\Models\LoanStatus::LOAN_STATUS_OPEN);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('approved_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan');
    }
}
