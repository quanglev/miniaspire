<?php

namespace Database\Seeders;

use App\Models\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [
                'name' => 'quangclient',
                'email' => 'quangclient@email.com',
                'role_id' => UserRole::USER_ROLE_CLIENT,
                'password' => Hash::make('12345678')
            ],
            [
                'name' => 'testclient',
                'email' => 'client@email.com',
                'role_id' => UserRole::USER_ROLE_CLIENT,
                'password' => Hash::make('12345678')
            ],
            [
                'name' => 'testemployee',
                'email' => 'employee@email.com',
                'role_id' => UserRole::USER_ROLE_EMPLOYEE,
                'password' => Hash::make('12345678')
            ],
            [
                'name' => 'admin',
                'email' => 'admin@email.com',
                'role_id' => UserRole::USER_ROLE_ADMIN,
                'password' => Hash::make('12345678')
            ]
        ]);
    }
}
