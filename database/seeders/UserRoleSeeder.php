<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_role')->insert([
            [
                'id' => UserRole::USER_ROLE_ADMIN,
                'code' => 'ADMIN'
            ],
            [
                'id' => UserRole::USER_ROLE_CLIENT,
                'code' => 'CLIENT'
            ],
            [
                'id' => UserRole::USER_ROLE_EMPLOYEE,
                'code' => 'EMPLOYEE'
            ],
        ]);
    }
}
