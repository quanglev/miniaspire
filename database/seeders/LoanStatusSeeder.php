<?php

namespace Database\Seeders;

use App\Models\LoanStatus;
use Illuminate\Database\Seeder;

class LoanStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('loan_status')->insert([
            [
                'id' => LoanStatus::LOAN_STATUS_OPEN,
                'code' => 'OPEN'
            ],
            [
                'id' => LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED,
                'code' => 'WAITING FOR APPROVED'
            ],
            [
                'id' => LoanStatus::LOAN_STATUS_APPROVED,
                'code' => 'APPROVED'
            ],
            [
                'id' => LoanStatus::LOAN_STATUS_REJECTED,
                'code' => 'REJECTED'
            ],
        ]);
    }
}
