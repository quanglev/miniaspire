<?php

namespace Database\Seeders;

use App\Models\LoanTermUnit;
use Illuminate\Database\Seeder;

class LoanTermUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('loan_term_unit')->insert([
            [
                'id' => LoanTermUnit::LOAN_TERM_UNIT_WEEK,
                'code' => 'WEEK',
                'fixed_rate' => 12.00,
            ],
            [
                'id' => LoanTermUnit::LOAN_TERM_UNIT_MONTH,
                'code' => 'MONTH',
                'fixed_rate' => 12.00,
            ],
            [
                'id' => LoanTermUnit::LOAN_TERM_UNIT_YEAR,
                'code' => 'YEAR',
                'fixed_rate' => 12.00,
            ],
            [
                'id' => LoanTermUnit::LOAN_TERM_UNIT_DATE,
                'code' => 'DAY',
                'fixed_rate' => 12.00,
            ],
        ]);
    }
}
