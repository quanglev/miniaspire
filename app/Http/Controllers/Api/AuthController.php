<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepositories\IUserRepository;
use App\Services\User\UserValidateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $userRepo;

    protected $userValidateService;

    public function __construct(IUserRepository $userRepo, UserValidateService $userValidateService)
    {
        $this->userRepo = $userRepo;
        $this->userValidateService = $userValidateService;
    }

    public function login(Request $request)
    {
        try {
            $validator = $this->userValidateService->getLoginValidator($request->all());

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->getMessages()
                ]);
            }

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Unauthorized'
                ]);
            }

            $user = $this->userRepo->getByEmail($request->email);

            if (!Hash::check($request->password, $user->password, [])) {
                throw new \Exception('Login Error.');
            }

            $token = $user->createToken('authToken')->plainTextToken;
            return response()->json([
                'message' => 'Logged in',
                'token' => $token,
                'token_type' => 'Bearer'
            ]);

        } catch (\Exception $error) {
            return response()->json([
                'message' => 'Login Error',
                'error' => $error
            ]);
        }
    }

    public function logout()
    {
        Auth::user()->currentAccessToken()->delete();
        foreach (Auth::user()->tokens as $token) {
            if ($token->name == 'authToken') {
                $token->delete();
            }
        }
        return response()->json([
            'message' => 'Logged out'
        ]);
    }
}
