<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepositories\IUserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userRepo;

    public function __construct(IUserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function index(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $users = $this->userRepo->getAll();

            return response()->json([
                'data' => $users
            ]);
        }

        return response()->json([
            'message' => 'Permission denied.'
        ]);
    }
}
