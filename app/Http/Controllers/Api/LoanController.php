<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\LoanRepositories\ILoanRepository;
use App\Repositories\LoanRepositories\IRepaymentRepository;
use App\Services\Loan\LoanValidateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    protected $loanRepo;

    protected $repaymentRepo;

    protected $loanValidateService;

    public function __construct(
        ILoanRepository $loanRepo,
        IRepaymentRepository $repaymentRepo,
        LoanValidateService $loanValidateService
    ) {
        $this->loanRepo = $loanRepo;
        $this->repaymentRepo = $repaymentRepo;
        $this->loanValidateService = $loanValidateService;
    }

    public function index(Request $request)
    {
        $loans = $this->loanRepo->getChangeableLoans();

        return response()->json([
            'data' => $loans
        ]);
    }

    public function store(Request $request)
    {
        if (Auth::user()->isEmployee()) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }

        $data = $request->all();
        $validator = $this->loanValidateService->getValidator($data);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->errors()->getMessages()
            ]);
        }

        $id = $this->loanRepo->create($data);

        return response()->json([
            'id' => $id
        ]);
    }

    public function update(Request $request, $id)
    {
        $loan = $this->loanRepo->getAChangeableLoan($id);
        if (!$loan || Auth::user()->isEmployee()) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }

        $inputs = $request->all();
        $validator = $this->loanValidateService->getValidator($inputs);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->errors()->getMessages()
            ]);
        }

        $data = $this->loanRepo->update($id, $inputs);

         return response()->json([
             'data' => $data
         ]);
    }

    public function destroy($id)
    {
        $loan = $this->loanRepo->getAChangeableLoan($id);

        // employee just approved the loan, dont have permission to delete
        if (!$loan || Auth::user()->isEmployee()) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }

        $result = $this->loanRepo->delete($id);

        return response()->json([
            'data' => $result
        ]);
    }

    public function submit($id)
    {
        $loan = $this->loanRepo->getAChangeableLoan($id);
        if (!$loan || Auth::user()->isEmployee()) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }

        return response()->json([
            'data' => $this->loanRepo->submitALoan($loan)
        ]);

    }

    public function approve($id)
    {
        $loan = $this->loanRepo->getAChangeableLoan($id);
        if (Auth::user()->isClient() || !$loan) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }

        \DB::beginTransaction();
        try {
            $this->loanRepo->approveALoan($loan);
            $repaymentData = $this->repaymentRepo->calculateRepayment($loan);
            $result = $this->repaymentRepo->createRepaymentRecords($repaymentData);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json([
                'error' => $e,
            ]);
        }

        return response()->json([
            'data' => $result
        ]);
    }

    public function reject($id)
    {
        $loan = $this->loanRepo->getAChangeableLoan($id);
        if (Auth::user()->isClient() || !$loan) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }

        return response()->json([
            'data' => $this->loanRepo->rejectALoan($loan)
        ]);
    }

    public function repayments($id)
    {
        $loan = $this->loanRepo->getAApprovedLoan($id);

        if (!$loan) {
            return response()->json([
                'message' => 'Permission denied'
            ]);
        }
        $repayments = $this->repaymentRepo->getByLoan($loan->id);

        return response()->json([
            'data' => $repayments
        ]);
    }
}
