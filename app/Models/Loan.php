<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends BaseModel
{
    use HasFactory;

    protected $table = 'loan';

    public function repayments()
    {
        $this->hasMany('App\Models\Repayment', 'loan_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\LoanStatus', 'status_id');
    }

    public function termUnit()
    {
        return $this->belongsTo('App\Models\LoanTermUnit', 'term_unit_id');
    }

    public function isApproved()
    {
        return $this->status->id == LoanStatus::LOAN_STATUS_APPROVED;
    }
}
