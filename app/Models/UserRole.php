<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserRole extends BaseModel
{
    public const USER_ROLE_ADMIN = 1;
    public const USER_ROLE_CLIENT = 2;
    public const USER_ROLE_EMPLOYEE = 3;

    use HasFactory;

    protected $table = 'user_role';
}
