<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanStatus extends Model
{
    public const LOAN_STATUS_OPEN = 1;
    public const LOAN_STATUS_WAITING_FOR_APPROVED = 2;
    public const LOAN_STATUS_APPROVED = 3;
    public const LOAN_STATUS_REJECTED = 4;

    use HasFactory;

    protected $table = 'loan_status';
}
