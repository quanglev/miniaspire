<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repayment extends BaseModel
{
    use HasFactory;

    protected $table = 'repayment';

    public function loan()
    {
        $this->belongsTo('App\Models\Loan', 'loan_id');
    }
}
