<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanTermUnit extends Model
{
    public const LOAN_TERM_UNIT_WEEK = 1;
    public const LOAN_TERM_UNIT_MONTH = 2;
    public const LOAN_TERM_UNIT_YEAR = 3;
    public const LOAN_TERM_UNIT_DATE = 4;

    use HasFactory;

    protected $table = 'loan_term_unit';
}
