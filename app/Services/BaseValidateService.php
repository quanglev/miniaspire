<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;

abstract class BaseValidateService extends BaseService
{
    public function getValidator(array $attributes)
    {
        $rules = $this->getRules();

        $messages = $this->getMessages();

        return Validator::make($attributes, $rules, $messages);
    }

    abstract protected function getRules();
    abstract protected function getMessages();
}
