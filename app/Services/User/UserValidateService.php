<?php

namespace App\Services\User;

use App\Services\BaseValidateService;
use Illuminate\Support\Facades\Validator;

class UserValidateService extends BaseValidateService
{
    protected function getRules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'password' => ['required']
        ];
    }

    protected function getMessages()
    {
        return [

        ];
    }

    public function getLoginValidator($data)
    {
        return Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required'
        ], []);
    }
}
