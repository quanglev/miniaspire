<?php

namespace App\Services\Loan;

use App\Services\BaseValidateService;
use Illuminate\Support\Facades\Validator;

class LoanValidateService extends BaseValidateService
{
    protected function getRules()
    {
        return [
            'amount' => ['required', 'numeric', 'max:999999.99'],
            'term' => ['required', 'integer', 'min: 2', 'max:20']
        ];
    }

    protected function getMessages()
    {
        return [

        ];
    }
}
