<?php

namespace App\Providers;

use App\Models\PersonalAccessToken;
use App\Repositories\IRepository;
use App\Repositories\LoanRepositories\ILoanRepository;
use App\Repositories\LoanRepositories\IRepaymentRepository;
use App\Repositories\LoanRepositories\LoanRepository;
use App\Repositories\LoanRepositories\RepaymentRepository;
use App\Repositories\Repository;
use App\Repositories\UserRepositories\IUserRepository;
use App\Repositories\UserRepositories\UserRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IRepository::class, Repository::class);
        $this->app->singleton(ILoanRepository::class, LoanRepository::class);
        $this->app->singleton(IUserRepository::class, UserRepository::class);
        $this->app->singleton(IRepaymentRepository::class, RepaymentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Sanctum::usePersonalAccessTokenModel(PersonalAccessToken::class);
    }
}
