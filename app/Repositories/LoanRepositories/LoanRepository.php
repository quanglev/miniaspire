<?php

namespace App\Repositories\LoanRepositories;

use App\Models\Loan;
use App\Models\LoanStatus;
use App\Models\LoanTermUnit;
use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LoanRepository extends Repository implements ILoanRepository
{
    public function getModel()
    {
        return Loan::class;
    }

    public function create($attributes = [])
    {
        $attributes['user_id'] = Auth::user()->id;
        $attributes['term_unit_id'] = LoanTermUnit::LOAN_TERM_UNIT_WEEK;
        $attributes['status_id'] = LoanStatus::LOAN_STATUS_OPEN;
        $attributes['created_at'] = Carbon::now();
        $attributes['approver_id'] = null;
        $attributes['approved_at'] = null;

        return parent::create($attributes);
    }

    public function getAChangeableLoan($id)
    {
        $inputs = ['id' => $id];

        if (Auth::user()->isClient()) {
            $inputs['status_id'] = LoanStatus::LOAN_STATUS_OPEN;
            $inputs['user_id'] = Auth::user()->id;
        }

        if (Auth::user()->isEmployee()) {
            $inputs['status_id'] = LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED;
        }

        return $this->getQueryByInputs($inputs)->first();
    }

    public function getAApprovedLoan($id)
    {
        $inputs = [
            'id' => $id,
            'status_id' => LoanStatus::LOAN_STATUS_APPROVED
        ];

        if (Auth::user()->isClient()) {
            $inputs['user_id'] = Auth::user()->id;
        }

        if (Auth::user()->isEmployee()) {
            $inputs['approver_id'] = Auth::user()->id;
        }

        return $this->getQueryByInputs($inputs)->first();
    }

    public function getChangeableLoans()
    {
        $inputs = [];

        if (Auth::user()->isClient()) {
            $inputs['status_id'] = LoanStatus::LOAN_STATUS_OPEN;
            $inputs['user_id'] = Auth::user()->id;
        }

        if (Auth::user()->isEmployee()) {
            $inputs['status_id'] = LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED;
        }

        return $this->getQueryByInputs($inputs)->get();
    }

    protected function getQueryByInputs(array  $inputs = [])
    {
        $query = Loan::query();

        if (array_key_exists('id', $inputs)) {
            $query->where('id', $inputs['id']);
        }

        if (array_key_exists('status_id', $inputs)) {
            $query->where('status_id', $inputs['status_id']);
        }

        if (array_key_exists('user_id', $inputs)) {
            $query->where('user_id', $inputs['user_id']);
        }

        if (array_key_exists('approver_id', $inputs)) {
            $query->where('approver_id', $inputs['approver_id']);
        }

        return $query;
    }

    public function submitALoan($loan)
    {
        $loan->status_id = LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED;
        return $loan->save();
    }

    public function approveALoan($loan)
    {
        $loan->status_id = LoanStatus::LOAN_STATUS_APPROVED;
        $loan->approver_id = Auth::user()->id;
        $loan->approved_at = Carbon::now();
        return $loan->save();
    }

    public function rejectALoan($loan)
    {
        $loan->status_id = LoanStatus::LOAN_STATUS_REJECTED;
        $loan->approver_id = Auth::user()->id;
        $loan->approved_at = Carbon::now();
        return $loan->save();
    }
}
