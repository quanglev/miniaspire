<?php

namespace App\Repositories\LoanRepositories;

use App\Models\Loan;

interface ILoanRepository
{
    public function getChangeableLoans();

    public function getAChangeableLoan($id);

    public function getAApprovedLoan($id);

    public function submitALoan(Loan $loan);

    public function approveALoan(Loan $loan);

    public function rejectALoan(Loan $loan);
}
