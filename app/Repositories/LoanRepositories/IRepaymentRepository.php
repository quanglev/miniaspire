<?php

namespace App\Repositories\LoanRepositories;

use App\Models\Loan;

interface IRepaymentRepository
{
    public function calculateRepayment(Loan $loan);

    public function createRepaymentRecords(array $data);

    public function getByLoan($loanId);
}
