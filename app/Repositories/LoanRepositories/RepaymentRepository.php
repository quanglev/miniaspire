<?php

namespace App\Repositories\LoanRepositories;

use App\Models\Loan;
use App\Models\LoanTermUnit;
use App\Models\Repayment;
use App\Repositories\Repository;
use Carbon\Carbon;

class RepaymentRepository extends Repository implements IRepaymentRepository
{
    public function calculateRepayment(Loan $loan)
    {
        $data = [];

        if ($loan->isApproved()) {
            $now = Carbon::now();
            $amount = $loan->amount;
            $term = $loan->term;
            $basePrincipal = $amount / $term;
            $termUnit = $loan->termUnit;
            $rate = $this->getRateByTernUnit($termUnit);

            for ($i = 0; $i <= $term; $i ++) {
                $remainingPrincipal = $amount - ($basePrincipal * $i);
                $principal = $i == 0 ? 0 : $basePrincipal;
                $interest = $i == 0 ? 0 : ($remainingPrincipal + $principal) * $rate;
                $payment = [
                    'loan_id' => $loan->id,
                    'remaining_principal' => number_format($remainingPrincipal, 2),
                    'principal' => number_format($principal, 2),
                    'interest' => number_format($interest, 2),
                    'total_principal_interest' => number_format($principal + $interest, 2),
                    'start_date' => $now->format('Y-m-d'),
                ];
                $payment['end_date'] = $now->addDays($this->getDaysByTermUnit($termUnit))
                    ->format('Y-m-d');
                $data[] = $payment;
            }
        }

        return $data;
    }

    public function createRepaymentRecords(array $data)
    {
        return \DB::table('repayment')->insert($data);
    }

    public function getByLoan($loanId)
    {
        return Repayment::query()->where('loan_id', $loanId)->get();
    }

    public function getModel()
    {
        return Repayment::class;
    }

    private function getRateByTernUnit(LoanTermUnit $termUnit)
    {
        switch ($termUnit->id) {
            case LoanTermUnit::LOAN_TERM_UNIT_WEEK:
            default:
                return $termUnit->fixed_rate / 5200;
        }
    }

    private function getDaysByTermUnit(LoanTermUnit $termUnit)
    {
        switch ($termUnit->id) {
            case LoanTermUnit::LOAN_TERM_UNIT_WEEK:
            default:
                return 7;
        }
    }
}
