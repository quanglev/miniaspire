<?php

namespace App\Repositories\UserRepositories;

interface IUserRepository
{
    public function getByEmail($mail);
}
