<?php

namespace App\Repositories\UserRepositories;

use App\Models\User;
use App\Repositories\Repository;

class UserRepository extends Repository implements IUserRepository
{
    public function getModel()
    {
        return User::class;
    }

    public function getByEmail($email)
    {
        return User::where('email', $email)->first();
    }
}
