##Introduction
Build a simple website that allows an authenticated user to go through a loan
application (doesn’t have to contain too many fields, but at least “amount
required” and “loan term”). All the loans will be assumed to have a “weekly”
repayment frequency.
After the loan is approved the user must be able to submit the weekly loan
repayments. It can be a simple repay button, which won’t need to check if
the dates are correct, but will just set the weekly amount to be repaid.

## Required
- php >= 7.3
- mysql

## Install

- Run the cmd to install vendor
    >composer install

- Create and config .env file

- Run the cmd to make the migration
    >php artisan migrate
                                                                    
    >php artisan db:seed 
- Start the server
    >php artisan serve
    
- Run unit test
    >php artisan test

##Apis list
Note: {token} for authenticated user, put it in header Authorization.
```
Authorization: Bearer 1|Oyvgv3Gy3EzxFtt5zReYvpOHSVXjBhSakkqUzASQ
```
|Description|Api|Method|Data|Note
|---|---|---|---|---|
|Login|api/login|Post| {email, password}|
|Get editable Loan|api/loans|Get|{token}|Client can get loan to edit or delete, employee get loan to approve or reject
|Create Loan|api/loans|Post| {token,amount,term}|
|Update Loan|api/loans/{id}/|Put| {token,amount,term}| Log as Client user, for Loan not submit yet
|Delete Loan|api/loans/{id}/delete|Delete|{token} |Log as Client user, for Loan not submit yet
|Submit Loan|api/loans/{id}/submit|Get|{token} |Log as Client user, for Loan not submit yet
|Approve Loan|api/loans/{id}/approve|Get|{token} |Log as Employee user, for submitted loan
|Reject Loan|api/loans/{id}/reject|Get|{token} |Log as Employee user, for submitted loan
|Get Repayments of approved loan|api/loans/{id}/repayments|Get|{token} |Log as Employee and Client user, for approved loan
|Logout|api/logout|Get| {token}|For logged in user
|Get Users list|api/users|Get|{token}|Log as Admin user, just for test

## Users test 
|email|password|user role
|---|---|---
admin@email.com|12345678|admin
client@email.com|12345678|Client
quangclient@email.com|12345678|Client
employee@email.com|12345678|employee

## User stories
Calculate repayment schedule with declining balance. Fixed rate 12% per year (it is set in database and changable)
###### For client
As client user can create a loan with amount and term. At moment term just support for weekly repayment, but it can be extended to daily, monthly or yearly.

After a loan is created, client can update, delete and submit a loan.

If a loan is approved, client can get the repayments schedule.

Client can get editable loans.

Client can not access to other client's loan.

Client can not approve or reject the loan.

###### For employee
As employee user can approve or reject a loan, just for the submitted loan.

If a loan is approved, the repayments are created auto following.

Employee can get the repayments to re-check
##### For admin

As a admin user can do anything just for test and monitor the system




                                                                                                           




