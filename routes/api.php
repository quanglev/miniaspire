<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'App\Http\Controllers\Api\AuthController@login');

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/logout', 'App\Http\Controllers\Api\AuthController@logout');

    Route::get('/users', [
        'uses' => 'App\Http\Controllers\Api\UserController@index',
        'as' => 'api.users.index'
    ]);

    Route::get('/loans', [
        'uses' => 'App\Http\Controllers\Api\LoanController@index',
        'as' => 'api.loans.index'
    ]);

    Route::post('/loans', [
        'uses' => 'App\Http\Controllers\Api\LoanController@store',
        'as' => 'api.loans.store'
    ]);

    Route::put('/loans/{id}', [
        'uses' => 'App\Http\Controllers\Api\LoanController@update',
        'as' => 'api.loans.update'
    ]);

    Route::delete('/loans/{id}', [
        'uses' => 'App\Http\Controllers\Api\LoanController@destroy',
        'as' => 'api.loans.destroy'
    ]);

    Route::get('/loans/{id}/submit', [
        'uses' => 'App\Http\Controllers\Api\LoanController@submit',
        'as' => 'api.loans.submit'
    ]);

    Route::get('/loans/{id}/approve', [
        'uses' => 'App\Http\Controllers\Api\LoanController@approve',
        'as' => 'api.loans.approve'
    ]);

    Route::get('/loans/{id}/reject', [
        'uses' => 'App\Http\Controllers\Api\LoanController@reject',
        'as' => 'api.loans.reject'
    ]);

    Route::get('/loans/{id}/repayments', [
        'uses' => 'App\Http\Controllers\Api\LoanController@repayments',
        'as' => 'api.loans.repayments'
    ]);

});
