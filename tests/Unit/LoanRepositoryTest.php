<?php

namespace Tests\Unit;

use App\Models\Loan;
use App\Repositories\LoanRepositories\LoanRepository;
use PHPUnit\Framework\TestCase;

class LoanRepositoryTest extends TestCase
{
    public function testGetModel()
    {
        $loanRepo = new LoanRepository();
        $data = $loanRepo->getModel();
        $this->assertEquals(Loan::class, $data);
    }
}
