<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function testLoginSuccess()
    {
        $this->seed();

        $response = $this->post('api/login', [
            'email' => 'client@email.com',
            'password' => '12345678'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => "Logged in",
            ]);
    }

    public function testLoginFail()
    {
        $this->seed();
        // wrong email and password
        $response = $this->post('api/login', [
            'email' => 'client1@email.com',
            'password' => '123456789'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => "Unauthorized",
            ]);
    }
}
