<?php

namespace Tests\Feature;

use App\Models\Loan;
use App\Models\LoanStatus;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoansApiTest extends TestCase
{
    use RefreshDatabase;

    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testClientGetOpenLoans()
    {
        $this->seed();

        //create 12 loans for client (10 open and 2 waiting for approved)
        $client = User::query()->where('name', 'testclient')->first();
        $clientLoansCount = 10;
        Loan::factory()->count($clientLoansCount)->state([
            'user_id' => $client->id,
            'status_id' => LoanStatus::LOAN_STATUS_OPEN
        ])->create();
        Loan::factory()->count(2)->state([
            'user_id' => $client->id,
            'status_id' => LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED
        ])->create();

        //create 20 loans for quang
        $quangClient = User::query()->where('name', 'quangclient')->first();
        $quangLoansCount = 20;
        Loan::factory()->count($quangLoansCount)->state([
            'user_id' => $quangClient->id,
            'status_id' => LoanStatus::LOAN_STATUS_OPEN
        ])->create();


        // Login as client
        $token1 = $client->createToken('authToken')->plainTextToken;
        $response1 = $this->withHeaders([
            'Authorization' => "Bearer {$token1}",
        ])->json('GET', '/api/loans', []);

        // Expect to see just 10 loans of client user
        $response1
            ->assertStatus(200)
            ->assertJsonCount($clientLoansCount, 'data');
    }

    public function testEmployeeGetSubmittedLoans()
    {
        $this->seed();

        //create 12 loans for client (10 open and 2 waiting for approved)
        $client = User::query()->where('name', 'testclient')->first();
        Loan::factory()->count(2)->state([
            'user_id' => $client->id,
            'status_id' => LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED
        ])->create();

        $employee = User::query()->where('name', 'testemployee')->first();
        $token1 = $employee->createToken('authToken')->plainTextToken;
        $response1 = $this->withHeaders([
            'Authorization' => "Bearer {$token1}",
        ])->json('GET', '/api/loans', []);

        // Expect to see just 10 loans of client user
        $response1
            ->assertStatus(200)
            ->assertJsonCount(2, 'data');
    }

    public function testSubmitLoan()
    {
        $this->seed();
        $client = User::query()->where('name', 'testclient')->first();
        $factoryLoan = Loan::factory()->state([
            'user_id' => $client->id,
            'status_id' => LoanStatus::LOAN_STATUS_OPEN
        ])->create();

        $token1 = $client->createToken('authToken')->plainTextToken;
        $response1 = $this->withHeaders([
            'Authorization' => "Bearer {$token1}",
        ])->json('GET', "/api/loans/{$factoryLoan->id}/submit", []);

        $response1
            ->assertStatus(200)->assertJson(['data' => true]);

        $loan = Loan::find($factoryLoan->id);
        $this->assertTrue($loan->status_id == LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED);
    }

    public function testRejectLoan()
    {
        $this->seed();
        $client = User::query()->where('name', 'testclient')->first();
        $factoryLoan = Loan::factory()->state([
            'user_id' => $client->id,
            'status_id' => LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED
        ])->create();

        $employee = User::query()->where('name', 'testemployee')->first();
        $token1 = $employee->createToken('authToken')->plainTextToken;
        $response1 = $this->withHeaders([
            'Authorization' => "Bearer {$token1}",
        ])->json('GET', "/api/loans/{$factoryLoan->id}/reject", []);

        $response1
            ->assertStatus(200)->assertJson(['data' => true]);

        $loan = Loan::find($factoryLoan->id);
        $this->assertTrue($loan->status_id == LoanStatus::LOAN_STATUS_REJECTED);
    }

    public function testApproveLoan()
    {
        $this->seed();
        $client = User::query()->where('name', 'testclient')->first();
        $factoryLoan = Loan::factory()->state([
            'user_id' => $client->id,
            'status_id' => LoanStatus::LOAN_STATUS_WAITING_FOR_APPROVED
        ])->create();

        $employee = User::query()->where('name', 'testemployee')->first();
        $token1 = $employee->createToken('authToken')->plainTextToken;
        $response1 = $this->withHeaders([
            'Authorization' => "Bearer {$token1}",
        ])->json('GET', "/api/loans/{$factoryLoan->id}/approve", []);

        $response1
            ->assertStatus(200)->assertJson(['data' => true]);
        $loan = Loan::find($factoryLoan->id);
        $this->assertTrue($loan->status_id == LoanStatus::LOAN_STATUS_APPROVED);
    }
}
